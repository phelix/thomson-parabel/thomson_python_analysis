"""
Created July 2021

@author: Tina Ebert, tebert@ikp.tu-darmstadt.de
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt
from scipy import optimize
from scipy.stats import binned_statistic
import csv
import PIL
from PIL import Image

## ---- overall REMARKS ----- ###
# - set variable 'path' to the directory containing the images to be processed
# - since the scanner adds a time-stamp to each file name, each image is called separately
# - read images as *.tif files, but be careful not to use the compressed ones given by the scanner.
#   instead, reading the *.img files with ImageJ and saving them as *.tif works well.
#   since there were some problems with reading the already converted psl-files with python, the psl conversion is included in this script
# - the main function is 'read_and_process_images()' in which both the binning and fitting function can be called for each set of scans


######### FUNCTIONS REQUIRING SCANNER PROPERTIES -> use this Scanner class #################

class Scan:
    def __init__(self, file_name,image, latitude, sensitivity,resolution,gradation,compression_type):
        self.file_name = file_name # the name
        self.image = image
        self.l = latitude # the latitude of the scanner
        self.s = sensitivity # the sensitivity of the scan
        self.r = resolution # the resolution of the scan in µm
        self.G = gradation #8bit: G=255; 16bit: G=65535
        self.compression_type = compression_type #"exp" or "lin"

    @classmethod
    def init_from_properties(cls, file_name,image, latitude, sensitivity,resolution,gradation,compression_type):
        return cls(file_name,latitude, sensitivity,resolution,gradation,compression_type)
         
    @classmethod
    def init_from_file(cls, file_name, compression_type):
        # read the .inf file containing the scan infos
        inf_file = open(file_name+'.inf', 'r')
        lines = inf_file.readlines()

        count = 1
        relevant_parts = []
        # get the relevant lines from the .inf file
        for line in lines:
            if count > 3 and count < 11:
                relevant_parts.append(line.strip())
            count += 1

        relevant_parts =  [int(x) for x in relevant_parts]
        size_x = relevant_parts[3] # size of the scanned image in x-direction
        size_y = relevant_parts[4] # size of the scanned image in y-direction
        resolution = relevant_parts[0] # scanner resolution
        S = relevant_parts[5] # sensitivity
        bits = relevant_parts[2] # bit depth
        L = relevant_parts[6] # latitude

        # calculate the maximum aplitude from the bit depth
        G=pow(2,bits)-1

        fid = open(file_name+'.img', 'rb')
        data = np.fromfile(fid, np.dtype('>u2'))
        image = data.reshape((size_y,size_x))
        
        return cls(file_name,image,L,S,resolution,G,compression_type)

    def convert_and_save_psl(self):
        im_psl = self.bit_to_psl()
        im_psl = Image.fromarray(im_psl)
        im_psl.save(self.file_name + "_PSL.tif", "TIFF")


    #convert bit image to psl reversing exponential compression
    def bit_to_psl(self):
        im = self.image
        match self.compression_type:
            case "exp":
                psl = pow(self.r/100,2)*pow(10,self.l*(im/self.G-0.5))*(4000/self.s)
            case "lin":
                psl = pow(im/self.G,2)*pow(self.r/100,2)*pow(10,self.l/2)*(4000/self.s)
            case _: 
                print('Currently only exponential decompression implemented.')
                psl = None
        return psl

    #calculate the scanner function for multiple scans (explanation of procedure: see PhD thesis of D. Rusby (2017), p. 72ff.)
    def calc_scan_fct(self, all_image_names,no_scans,path,shot_ID): 
        #images: array of all scans in chronological order (1st scan 1st)
        #path: path where histograms will be saved
        img_names=[]
        for i in range(len(all_image_names)-1):
            img_names.append(path+all_image_names[i+1])
        self.calc_hist(path+all_image_names[0],img_names,path,shot_ID)
        lin_opt=self.fit_lin_fct(path,shot_ID,no_scans)
        return lin_opt

    #HELPER
    def calc_hist(self,img1_name,image_names,path,shot_ID):
        print("> Calculate histogram")
        #arrays to accumulate values of all images
        all_scans=[]
        all_bins_PSL=[]
        all_hist_PSL=[]

        ######## process scan 1 - used as reference for binning (see manual/description of routine) ###
        print("> Process scan 1")
        #read image
        img1=np.loadtxt(img1_name)
        #reshape 2D array to 1D
        scan1_PSL=img1.flatten()
        #remove all saturated values and zeros
        #get highest possible PSL value -> depends on scanner
        max_val=np.power(10,(self.l*(65530/self.G-0.5)))*4000/self.s*np.power(self.r/100,2)
        idx_saturated=np.argwhere(scan1_PSL>max_val)
        idx_zeros=np.argwhere(scan1_PSL==0)
        scan1_PSL=np.delete(scan1_PSL,idx_saturated)
        scan1_PSL=np.delete(scan1_PSL,idx_zeros)
        #get random set of pixels
        thresh_no=350000
        if scan1_PSL.size > thresh_no:
            indices=np.random.randint((scan1_PSL.size-idx_saturated.size-idx_zeros.size),size=thresh_no)
            scan1_PSL=scan1_PSL[indices]
        else:
            indices=np.arange(0,scan1_PSL.size)

        ######## process all other scans (same steps as before) #####################################
        print("> Process all other scans")
        for i in range(len(image_names)):
            print("> Flatten arrays")
            img_i=np.loadtxt(image_names[i])
            scan_PSL=img_i.flatten()
            scan_PSL=np.delete(scan_PSL,idx_saturated)
            scan_PSL=np.delete(scan_PSL,idx_zeros)
            scan_PSL=scan_PSL[indices]
            all_scans.append(scan_PSL)

            print("> Binning")
            hist_PSL,edges,_=binned_statistic(scan1_PSL,scan_PSL,'mean',bins=25)
            bins_PSL=edges[:-2]
            #remove 'nan'
            bins_PSL=np.delete(bins_PSL,np.argwhere(np.isnan(hist_PSL[:-1])))
            hist_PSL=np.delete(hist_PSL,np.argwhere(np.isnan(hist_PSL[:-1])))
        
            all_bins_PSL.append(bins_PSL)
            all_hist_PSL.append(hist_PSL[:-1]) #remove last (saturated) value

        #save data
        print("> Write csv files")
        #pixels that were used for creating the histograms
        np.savetxt(path+str(shot_ID)+"_scans.csv",all_scans,fmt='%1.4e')
        #file format: column 0--n/2: bins; column n/2+1--n: hist
        np.savetxt(path+str(shot_ID)+"_hist_PSL.csv",np.vstack((all_bins_PSL,all_hist_PSL)),fmt='%1.8e')
        return

    #HELPER
    def lin_fct(self,x,m):
        return m*x

    #HELPER
    def fit_lin_fct(self,path,shot_ID,no_scans):
        all_hist=[]
        all_lin_opt=[]

        #read relevant hist data from csv file
        data=np.loadtxt(path+str(shot_ID)+"_hist_PSL.csv")
        bins=data[0,:]

        #load hists in reverse order
        for i in range(no_scans-1):
            #idx: 2*(no_scans-1)-1-i 
            #(number of additional scans: no_scans-1; 
            #total number of columns: 2*(no_scans-1); 
            #python indexing starts with 0 -> subtract 1)
            hist=data[2*no_scans-3-i,:] 
            all_hist.append(hist)
    
            #fit linear function
            lin_opt, lin_cov = optimize.curve_fit(self.lin_fct,bins,hist)
            all_lin_opt.append(lin_opt)

        #save fit parameters
        with open(path+str(shot_ID)+"_lin_fit.csv", 'w',newline='') as writeFile:
            for i in range(no_scans-1):
                writer = csv.writer(writeFile,delimiter='\t')
                writer.writerow([str(no_scans-i),all_lin_opt[i][0]])
    
        #plot and save data and linear functions
        for i in range(no_scans-1):
            plt.plot(bins,self.lin_fct(bins,*all_lin_opt[i]))
            plt.scatter(bins,all_hist[i],label='n='+str(no_scans-i))
            plt.legend(loc='upper left', borderaxespad=0.5)
        plt.title(str(shot_ID))
        plt.savefig(path+str(shot_ID)+".png")
        plt.close()

        return all_lin_opt

##################### IP FADING CORRECTION ##################################################

#correct PSL values for fading between shot and scan
#img_PSL : PSL image or array
#time : time between shot and scan in minutes
#radiation_type : 'ion' or 'xray' - used to choose corresponding fading functions
#IP_type : type of IP (currently implemented: 'SR' and 'TR')
#data_dimension : this function can correct 1D arrays (e.g. a spectrum) or 2D arrays (e.g. an image) 
def fading_correction(img_PSL,time,radiation_type,IP_type,data_dimension=2):
    #--- create array same size as input image
    img_PSL_t0=np.zeros(img_PSL.shape) 

    #--- get parameters of exponential fading functions depending on IP type and radiation type
    #Ions - Bonnet 2013
    if radiation_type=='ion':
        if IP_type=='SR':
            A=[0.49,0.51]
            B=[11.9,1390]
        elif IP_type=='TR':
            A=[0.49,0.51]
            B=[17.9,1482]
        else: 
            print('Not a valid IP type!')            

    #Xrays - Boutoux 2015
    elif radiation_type=='xray':
        if IP_type=='TR':
            A=[0.535,0.465]
            B=[23.812,3837.2]
        elif IP_type=='SR':
            A=[0.579,0.421]
            B=[15.052,3829.5]
        else: 
            print('Not a valid IP type!')
    else:
        print('Not a valid radiation type!')

    #calculate fading factor
    s=0 
    for c in zip(A,B):
        s+=c[0]*np.exp(-time/c[1])

    #--- calculate PSL value at time of shot (t=0) for each img_PSL entry
    if data_dimension==1:
        for i in np.arange(img_PSL.size):
            #get PSL value at scan time
            PSL_0=img_PSL[i] 
            #divide PSL value at scan time by fading factor for correction
            img_PSL_t0[i]=PSL_0/s
    elif data_dimension==2:
        for i in np.arange(img_PSL.shape[0]):
            for j in np.arange(img_PSL.shape[1]):
                #get PSL value at scan time
                PSL_0=img_PSL[i,j] 
                #divide PSL value at scan time by fading factor for correction
                img_PSL_t0[i,j]=PSL_0/s
    else: print('Invalid dimension of PSL input (can be 1D or 2D array)')

    return img_PSL_t0



############# SCAN RECOVERY AFTER MULTIPLE SCANS ############################
# this part of the script is not finished yet, but might be useful for later development
#--- FURTHER ACTION ----- #
# - for each series of scans the slopes of the linear fit b_n=m_n*b1 are saved in "label_lin_fit.csv"
# - average m_n over the different series (e.g. in Excel)
# - fit function of form a*n^c to create function m(n)

#read images and calculate histograms for each scan
#path : path to images
#image_labels : name of images without scan number
#no_of_scans : number of consecutive scans of the same IP
#scanner : initialized object of Scanner class
def read_and_process_images(path,image_labels,no_of_scans,scanner):
    for i,label in enumerate(image_labels):
        print("> "+label)
        print("> Read images")
        #read all images of scan series
        images=[]
        for j in np.arange(no_of_scans[i]):
            image_name=label+"_"+str(j)+".tif"
            img=cv2.imread(path+image_name,-1)
            images.append(img)
        img1=images[0] #first scan
        scanner.calculate_hist(img1,images,label)
        scanner.fit_linear_fct(label,no_of_scans[i])