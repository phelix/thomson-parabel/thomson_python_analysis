# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 10:07:40 2021

@author: Jonas Kohl(jkohl@ikp.tu-darmstadt.de)
"""
import ast

#-----------------------------------------------------------------------------------------------
# create a textfile containing the information for a shot
#----------------------------------------------------------------------------------------------

Eintrag = {
    'path':         'E:/StrahlzeitYannikp190/12072021/',
    'filename':     '07-12-21_shot-21_PSL',
    'shot':         0,
    'IP':           'SR',
    'rot_deg':      90,
    'thresh_IP':    0.0005,
    'thresh_trace': 0.1,
    'E-field':      1e6,
    'E-length':      0.190,
    'E-drift':      0.034,
    'B-field':      0.875,
    'B-length':      0.120,
    'B-drift':      0.224,
    'time':20
    
    }

x = Eintrag.get("Path")

class config:
    def __init__(self):
        return
        
    def write_new_txt(self,path_txt, data_dict, file_name="noName"):
        #if no name is choosen, give the name data_dict.get("filename")+"_conf.txt"
        if file_name == "noName":
            file_name = data_dict.get("filename")+"_conf.txt"
        
        #create/open a txt file and write the information of the dict data_dict 
        with open(path_txt+file_name, "w") as txt_file:
            txt_file.write("Configfile for thomsonparabola routine\n\n")
            txt_file.writelines("Entry"+ "\n")
            txt_file.writelines("Loadingpath: "     + data_dict.get("path")+ "\n")
            txt_file.writelines("Filename: "        + data_dict.get("filename")+ "\n")
            txt_file.writelines("Shotnumber: "      + str(data_dict.get("shot"))+ "\n")
            txt_file.writelines("IPtype: "          + data_dict.get("IP")+ "\n")
            txt_file.writelines("rot_deg: "         + str(data_dict.get("rot_deg"))+ "\n")
            txt_file.writelines("thresh_IP: "       + str(data_dict.get("thresh_IP"))+ "\n")
            txt_file.writelines("thresh_trace: "    + str(data_dict.get("thresh_trace"))+ "\n")
            txt_file.writelines("E-Field Strength: "+ str(data_dict.get("E-field"))+ "\n")
            txt_file.writelines("E-Field length: "  + str(data_dict.get("E-length"))+ "\n")
            txt_file.writelines("E-Field drift: "   + str(data_dict.get("E-dift"))+ "\n")
            txt_file.writelines("B-Field Strength: "+ str(data_dict.get("B-field"))+ "\n")
            txt_file.writelines("B-Field length: "  + str(data_dict.get("B-length"))+ "\n")
            txt_file.writelines("B-Field drift: "   + str(data_dict.get("B-drift"))+ "\n")
            txt_file.writelines("Scan delay: "      + str(data_dict.get("time"))+ "\n")
            txt_file.writelines("Filter: "          + str(data_dict.get("filter"))+ "\n")
            txt_file.writelines("\n")
            txt_file.writelines("End of file")
        txt_file.close()
        return
    
    def find_entries(self,list_of_strings, codeword = "Entry", entrylength = 15):
        return_list = []
        iterator = 1
        for line in list_of_strings:
            if line.find(codeword) !=-1:
               return_list.append(list_of_strings[iterator:iterator+entrylength])
            iterator += 1
        return return_list
                                 
    
    def load_from_txt(self,path_txt,file_name,random_order = True):
        with open(path_txt+file_name, "r") as txt_file:
            txt_lines = txt_file.readlines()
        txt_file.close()
        entries = self.find_entries(txt_lines)
        return_dict = []
        for entry in entries:            
            if random_order:
                d = {}
                for line in entry:   
                    key,val = line.split(": ")
                    d[key] = val
                new_dict={
                    "path":d["Loadingpath"].strip(),
                    'filename':d["Filename"].strip(),
                    'shot':int(d["Shotnumber"]),
                    'IP':d["IPtype"].strip(),
                    'rot_deg':int(d["rot_deg"]),
                    'thresh_IP':float(d["thresh_IP"]),
                    'thresh_trace':float(d["thresh_trace"]),
                    'E-field':float(d["E-Field Strength"]),
                    'E-length':float(d["E-Field length"]),
                    'E-drift':float(d["E-Field drift"]),
                    'B-field':float(d["B-Field Strength"]),
                    'B-length':float(d["B-Field length"]),
                    'B-drift':float(d["B-Field drift"]),
                    'time':float(d["Scan delay"]),
                    'filter':ast.literal_eval(d["Filter"])
                    }
            else:
                path=           entry[0].split(": ")[1]
                filename=       entry[1].split(": ")[1]
                shot=           int(entry[2].split(": ")[1])
                ip=             entry[3].split(": ")[1]
                rot_deg=        int(entry[4].split(": ")[1])
                thresh_IP=      float(entry[5].split(": ")[1])
                thresh_trace=   float(entry[6].split(": ")[1])
                Efield=         float(entry[7].split(": ")[1])
                Elength=         float(entry[8].split(": ")[1])
                Edrift=         float(entry[9].split(": ")[1])
                Bfield=         float(entry[10].split(": ")[1])
                Blength=         float(entry[11].split(": ")[1])
                Bdrift=         float(entry[12].split(": ")[1])
                time=           float(entry[13].split(": ")[1])
                Filter =        ast.literal_eval(entry[14].split(": ")[1])
                new_dict = {"path":path,
                            'filename':filename,
                            'shot':shot,
                            'IP':ip,
                            'rot_deg':rot_deg,
                            'thresh_IP':thresh_IP,
                            'thresh_trace':thresh_trace,
                            'E-field':Efield,
                            'E-length':Elength,
                            'E-drift':Edrift,
                            'B-field':Bfield,
                            'B-length':Blength,
                            'B-drift':Bdrift,
                            'time':time,
                            'filter':Filter}
            return_dict.append(new_dict)
        return return_dict
    
    def add_to_txt(self,path_txt,file_name):
        print("to be implemented")
        with open (path_txt+file_name, "a") as txt_file:
            txt_file.writelines("platzhalter")
        
if __name__ == '__main__':
    #------------------------------------------------------------------------------
    # Testarea
    #------------------------------------------------------------------------------
    path_config = "./Thomson_config/"
    #write_new_txt(path_config,Eintrag,"testfile.txt")
    new = config()
    test2= new.load_from_txt("./Thomson_config/","testfile.txt",False)
    #test2=load_from_txt(path_config,"testfile.txt",random_order = True)
    #add_to_txt(path_config,"testfile.txt"
