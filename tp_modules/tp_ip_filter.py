# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 08:24:29 2021

@author: Jonas Kohl (jkohl@ikp.tu-darmstadt.de), Tina Ebert (tebert@ikp.tu-darmstadt.de)
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import csv
from lmfit import Model

#filepaths of SRIM Data (',' have to be changed to '.' in the files! This script contains a helper function (convert_comma_to_dot()), see below)
path_SRIM_Cu = './tp_modules/SRIM_outputs/Hydrogen in Copper.txt'
path_SRIM_Al = './tp_modules/SRIM_outputs/Hydrogen in Aluminum.txt'

#-----------------------------------------------------------------------------
# apply filter to energies in filter range
#-----------------------------------------------------------------------------
#MAIN
def energy_loss_in_filter_range(energy_in,energy_in_min,energy_in_max,material,thickness): 
    #thickness in mm
    #separating the energies in arrays below the lower limit, the filtered region and above the upper limit
    energies_filtered=energy_in[np.logical_and(energy_in>=energy_in_min,energy_in<=energy_in_max)]
    energies_below_limit=energy_in[energy_in<energy_in_min]
    energies_above_limit=energy_in[energy_in>energy_in_max]
    
    #calculating the shift in energy for the energies above
    energies_filtered = interpolate_energy_loss(energies_filtered,material,thickness)
    
    #combine the changed values with the unchanged values
    energies_out = np.concatenate((energies_below_limit,energies_filtered,energies_above_limit))
    
    return energies_out

#get energy loss in filter by interpolating SRIM energy loss data
def interpolate_energy_loss(energy_in,material,filter_thickness):    
    if material=='Cu':
        path_filter=path_SRIM_Cu
    elif material=='Al':
        path_filter=path_SRIM_Al
    else:
        #print('Material not yet in available filter list. Please add in TP_IP_filter.py')
        print('This material is a nonsense idea...')
        print('No, seriously. You have to add it in TP_IP_filter.py')
    
    #read SRIM Data (by now the ',' in the data has already been changesd to '.')
    SRIM_data = np.genfromtxt(path_filter,skip_header=23,skip_footer=13)
    #conversion factor: MeV/(mg/cm2) nach MeV/mm: read from SRIM output file
    SRIM_file=[]
    with open(path_filter, 'r') as readFile:
        reader = csv.reader(readFile,delimiter=',') 
        for i, line in enumerate(reader):
            SRIM_file.append(line)
    readFile.close()
    cf=float(SRIM_file[-8][0].split()[0]) #conversion factor for MeV/mm

    #setting the x and y data for the fit: x=Energies, y=sum of attentuation coeff.
    x=SRIM_data[:,0]
    y=(SRIM_data[:,2]+SRIM_data[:,3])*cf #sum of nuclear and electron stopping power in MeV/mm
    
    energy_out=[]
    for energy in energy_in:
        e=energy
        for i in np.arange(filter_thickness*1000): #1um steps
            dEdx=np.interp(e,x,y)*0.001 #energy loss for 1um
            e=e-dEdx
        if e<0:e=0
        energy_out.append(e)
            
    # plt.plot(energy_in,energy_in,label='in')
    # plt.plot(energy_in,energy_out,label='out')
    # plt.grid(True)
    # plt.legend()
    # plt.show()
    
    return energy_out

#Calculate energy loss in filter by fitting function to SRIM energy loss data
def calc_energy_loss(energy_in,material,filter_thickness):
    if material=='Cu':
        path_filter=path_SRIM_Cu
    elif material=='Al':
        path_filter=path_SRIM_Al
    else:
        print('Material not yet in available filter list. Please add in tp_ip_filter.py')
    #calc attenuation coefficient from SRIM data
    a,b=calc_attenuation_coeff(path_filter)
    
    #constants during calculations (integral can be written as (E_0^(b+1)-x*a*(b+1))^(1/(b+1)) with S(E)=a*E^-b and x=filter_thickness)
    c = b+1
    d = 1/c
    c1=filter_thickness*a*c
    
    #energy loss in filter
    energy_out = (energy_in**c-c1)**d
    return energy_out


#Calculate the Parameter for dE/dx = a*E^-b for Attenuatuion in MeV/mm
def calc_attenuation_coeff(path_filter):
    #read SRIM Data (by now the ',' in the data should have been changed to '.')
    SRIM_data = np.genfromtxt(path_filter,skip_header=23,skip_footer=13)

    #conversion factor: MeV/(mg/cm2) nach MeV/mm: read from SRIM output file
    SRIM_file=[]
    with open(path_filter, 'r') as readFile:
        reader = csv.reader(readFile,delimiter=',') 
        for i, line in enumerate(reader):
            SRIM_file.append(line)
    readFile.close()
    cf=float(SRIM_file[-8][0].split()[0])

    #set the x and y data for the fit: x=Energies, y=sum of attentuation coeff.
    x=SRIM_data[:,0]
    y=(SRIM_data[:,2]+SRIM_data[:,3])*cf #sum of nuclear and electron stopping power
        
    #fit power law to srim differential energy loss data
    params = curve_fit(power_law_fct, x, y)
    a,b = params[0]
    return a,b


#-----------------------------------------------------------------------------
# HELPER functions
#-----------------------------------------------------------------------------
#HELPER convert comma to dot in e.g. SRIM files
def convert_comma_to_dot(Path):
    file = open(Path,'r')
    textinhalt = file.read()
    textinhalt=textinhalt.replace(',','.')
    file.close()
    file2= open(Path,'w')
    file2.write(textinhalt)
    file2.close()    
    
#power law function for fit
def power_law_fct(x, a, b):
    return a*x**-b

#HELPER
def plotEnergyloss(energy_in,energy_out):
    plt.plot(energy_in,energy_in-energy_out)
    plt.xlabel("Incoming Energy in MeV")
    plt.ylabel("Energieloss in Filter in MeV")