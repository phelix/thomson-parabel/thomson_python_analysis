"""
Created July 2021

@author: Tina Ebert, tebert@ikp.tu-darmstadt.de, 

@modified: Johannes Hornung, j.hornung@gsi.de
"""

import numpy as np
import os
import csv
import cv2
from cv2 import ximgproc
from lmfit import Model
from matplotlib import pyplot as plt
from tp_modules.ip_scanner import fading_correction
from tp_modules.tp_ip_filter import energy_loss_in_filter_range
import scipy.constants as const

#----------Constants----------############
# q=1.6021766208e-19 #[C]
# m=1.660538921e-27*1.0079 #[kg], protons
c = const.speed_of_light
# c=299792585 #[m/s]

# later on, these values should be given in a config file!
# B=0.872 #[T] #0.857
# E=1e5 #[V/m]
# a=0.190*(0.190/2+0.034) # E_field_length*(E_field_length/2+E_drift) 
# b=0.120*(0.120/2+0.224) # B_field_length*(B_field_length/2+B_drift), for values see J. Ding Matlab script / Niels Master Thesis #dB=0.245?

# c1=q*B*b/np.sqrt(2*m)
# c3=np.power((q*B*b/np.sqrt(2*m)),-2)*np.power((q*E*a/2),2)/(m*c*c)
#c4=np.power(q*B*b/np.sqrt(2*m),-2)*q*E*a/2

px = 25e-06 #[m] #TODO: read IP resolution from variable / config file
upper_limit = 100 #MeV
margin = 40 #extents the roi (region of interest) when cropping the relevant image areas
margin_crop_rot = 75 #margin added before cropping the rotated image to have enough pixels for background subtraction

show_images = False # used for debugging!
show_images_loop = False

"""
Main class containing every property of the analysis

"""
class TP_Spectrum:
    def __init__(self,path,filename,IP_type,E_field_strength,E_field_length,E_field_drift,B_field_strength,B_field_length,B_field_drift,no_scans=1):
        self.path=path # path to the image
        self.filename=filename # name of the image
        self.IP_type=IP_type # type of the image plate
        self.modified_IP = None # used to store the modified IP
        self.E=E_field_strength # electric field strength of the TP in V/m

        self.B=B_field_strength # magnetic field strength of the TP in T

        self.a=E_field_length*(E_field_length/2+E_field_drift) # E_field_length*(E_field_length/2+E_drift) 
        self.b=B_field_length*(B_field_length/2+B_field_drift) # B_field_length*(B_field_length/2+B_drift)

        self.rot_deg=[0] # rotation angle of the IP
        self.crop_rect=[] # rectangle used to crop the IP
        self.efield=[0] # electric field
        self.zero=[[0,0]] # position of the zero / x-ray point

        self.no_scans=no_scans
        self.spectrum_last_scan=[] # for IPs with multiple scanning read last one

        self.spectrum_PSL=[] # spectrum in PSL
        self.spectrum_PSL_normalized=[] # normalized spectrum in PSL
        self.spectrum_PSL_at_t0=[] #calculated in correct_fading()
        self.spectrum_proton_numbers=[] #calculated in PSL_to_number()
        self.x_positions=[]
        self.energy_bins=[]
        self.energy_bin_widths=[]

        self.masks_for_spectra=[]

    """
    Main function 1
    Load image, flip and rotate if needed, use openCV to semi-automatically
    find and crop the main area using a given threshold.   

    Parameters
    ---------
    # rotation : can be 0, 90, 180 or 270  
    # flip_ud : flip image upside down (horizontally)
    # flip_rl: flip image left to right (vertically)
    # tresh: threshold 
    """
    def crop_IP(self,thresh=0.01,rotation=0,flip_ud=False,flip_rl=False):
        #load image
        img=cv2.imread(self.path+self.filename+'.tif',-1)
        
        #flip & rotate if required -> zero should end up in lower left corner
        if flip_ud: #up-down
            img=cv2.flip(img,0)
        if flip_rl: #left-right
            img=cv2.flip(img,1)
        if rotation!=0:
            if rotation==90:
                img=cv2.rotate(img,cv2.ROTATE_90_CLOCKWISE)
            elif rotation==180:
                img=cv2.rotate(img,cv2.ROTATE_180)
            elif rotation==270:
                img=cv2.rotate(img,cv2.ROTATE_90_COUNTERCLOCKWISE)
            else:
                print('> ERROR: invalid rotation angle. Possible Choices: 0,90,180,270')
                
        #find IP and crop it to make the image smaller
        # This could also be done manually with a GUI
        _,img_grey = cv2.threshold(img,thresh,255,0)#cv2.THRESH_BINARY
        img_cnt=img_grey.copy()
        img_cnt=cv2.GaussianBlur(img_grey,(11,11),0)#add blur to smooth out noisy plates
        img_cnt=img_cnt.astype(np.uint8)
        contours,hierarchy=cv2.findContours(img_cnt,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)[-2:] #the [-2:] makes sure this line works for all cv2 versions - in some cases the return statements differ   
        cnt = max(contours, key = cv2.contourArea)
        x,y,w,h = cv2.boundingRect(cnt)
        img_crop=img[y:y+h,x:x+w]
        cv2.imwrite(self.path+'analysis/'+self.filename+'_cropped_IP.tif',img_crop)
        self.modified_IP = img_crop
        del img,img_cnt,img_grey
        return

    """MAIN FUNCTION 2

    Parameters
    ----------
    fit :
        if True, fit E-field; else: use initialized E-field value of
        tp_spectrum object
    enhance_zero : lowers the threshold in the area around zero to
        achieve a better find Contours result
    rot_angles :
        rotation angles in degree

    threshold: anything below the threshold will be set to 0. max value is 255, default is 1.1

    plotting functions are for debugging
    """
    def rotate_and_fit_E_field(self,thresh=1.1,fit=True,enhance_zero=False,rot_angles=np.arange(-1.5,1.5,0.05)):        
        output_path=self.path+'analysis/'
        #create empty arrays
        all_params=[]
        all_perr=[]
        all_x=[]
        all_y=[]
        all_rect=[]
        all_zeros=[]         
        offset=0
    
        #read previous results, e.g. the cropped IP. Instead of save and load, it would also be possible to just use the 
        # modified ip from the previous step
        print("> Load image: "+output_path+self.filename+'_cropped_IP.tif')
        file, file_extension = os.path.splitext(self.filename)
        try:
            img=cv2.imread(output_path+self.filename+'_cropped_IP.tif',-1)
            self.modified_IP=img
            if show_images:
                plt.imshow(img)
                plt.title('cropped IP')
                plt.show()
        except:
            print('> ERROR: Image loading failed.')
            return

        ###########rotate image for optimization############
        # This could be highly optimized by keeping the zero position and trace points and just rotateing the corresponding points + fitting
        # instead of performing all image processing steps over and over!
        for d in rot_angles:
            print('rotating angle: ',d)
            rows,cols=img.shape
            dst = rotate(img,d,(cols/2,rows/2))
    
            #--- threshold and crop relevant area
            thresh_gray = thresholding(dst,thresh)
            if show_images and show_images_loop:
                plt.imshow(thresh_gray)
                plt.title('identify trace /zero by thresholding')
                plt.show()
            
            #convert all white pixels (which ideally are only the traces) of thresholded image to (x,y) points
            points = img_to_points(thresh_gray,255)
            if enhance_zero: #add points on leftmost side and add height margin to make sure zero is still in ROI when image is rotated
                x_min=0                
                y_min=int(min(points[:,1])-100)
                y_max=int(max(points[:,1])+100)
                if y_min < 0: y_min=0
                if y_max > thresh_gray.shape[0]: y_max=thresh_gray.shape[0]-1 #python indices start at 0
                points=np.concatenate((np.array([[x_min,y_min]]),points))
                points=np.concatenate((np.array([[x_min,y_max]]),points))
            if show_images and show_images_loop:
                plt.imshow(thresh_gray)
                plt.scatter(points[:,0],points[:,1])
                plt.title('show found trace points from thresholding')
                plt.axis('equal')
                plt.show()
            
            #crop image using the bounding rectangle of the white pixel points
            crop, rect = crop_img(thresh_gray,points,margin,margin)
            all_rect.append(rect)
            if show_images and show_images_loop:
                plt.imshow(thresh_gray)
                plt.scatter(points[:,0],points[:,1])
                plt.title('show first and last found points to identify cropping region')
                plt.show()

            #skeletonize
            skeleton=crop[offset:crop.shape[0]-offset,offset:crop.shape[1]-offset]
            kernel = np.ones((4,4),np.uint8)
            skeleton=cv2.GaussianBlur(crop[offset:crop.shape[0]-offset,offset:crop.shape[1]-offset],(15,15),0)
            skeleton=cv2.dilate(crop[offset:crop.shape[0]-offset,offset:crop.shape[1]-offset],kernel)
            skeleton=skeleton.astype(np.uint8)
            skeleton=cv2.ximgproc.thinning(skeleton)
            skeleton=skeleton[1:-1,1:-1] #remove edge effects 
            if show_images and show_images_loop:
                plt.imshow(skeleton,'gray', aspect='auto')
                plt.title('skeletonized trace')
                plt.show()           
            
            points_sk = img_to_points(skeleton,255)
            points_x = points_sk[:,0]
            points_y = points_sk[:,1] 
            if show_images and show_images_loop:
                plt.imshow(skeleton,'gray',aspect='auto')           
                plt.scatter(points_x,points_y)
                plt.title('points from skeleton')
                plt.show()

            #find zero
            if enhance_zero: # assume that zero is guessed because of weak signal -> apply lower threshold to left 1/6 of the image 
                thresh_gray_zero = thresholding(dst,0.03*thresh)
                crop_zero=thresh_gray_zero[rect[1]:(rect[1]+rect[3]),rect[0]:(rect[0]+rect[2])]               
                crop_zero = cv2.medianBlur(crop_zero,5) #blur to close gaps in blurry images
                zero, col_crop = find_zero_cnt(crop_zero[offset:crop.shape[0]-offset,offset:crop.shape[1]-offset])
            else:
                zero, col_crop = find_zero_cnt(crop[offset:crop.shape[0]-offset,offset:crop.shape[1]-offset])
            
            all_zeros.append([zero[0],zero[1]])
            if show_images and show_images_loop:   
                plt.imshow(col_crop, aspect='auto')
                plt.title('Highlight Zero')
                plt.show()
            
            #fit function
            x_0=zero[0]
            y_0=zero[1]
            self.zero=zero

            #choose proton parabola (largest feature in skeleton)
            points_x,points_y=find_proton_trace(skeleton)
            if show_images and show_images_loop:
                plt.figure(figsize=(15,5))
                plt.imshow(crop,'gray', aspect='auto')
                plt.scatter(points_x,points_y,s=1,color='r')
                plt.scatter(x_0,y_0,s=2)
                plt.title('show proton trace (should be largest feature in skeleton)')
                plt.show()

            #currently not working!
            if fit:
                #params, params_covariance = optimize.curve_fit(parabola_m, points_x[:-2]*px, points_y[:-2]*px,p0=[E])
                parabola_model=Model(parabola_m,independent_vars=['x_m', 'x0','y0'])#,B=self.B, charge=1, mass=1.0079)#, a=self.a, b=self.b)
                init_params=parabola_model.make_params(E_fit=self.E)
                #result=parabola_model.fit(points_y[:-2]*px,init_params,x_m=points_x[:-2]*px,x0=x_0,y0=y_0)
                result=parabola_model.fit(points_y*px,init_params,x_m=points_x*px,x0=x_0*px,y0=y_0*px, B=self.B, charge=1, mass=1.0079, a=self.a, b=self.b)
                if result.covar==None:
                    perr=[10000]
                    print('> - No covariance matrix')
                else:
                    perr = np.sqrt(np.diag(result.covar))
                all_perr.append(perr[0])
                all_params.append(result.best_values['E_fit'])
                E0=result.best_values['E_fit']
            else:
                E0=self.E
                all_params.append(E0)
                diff_arr=np.absolute(points_y*px-parabola_m(points_x*px,E0,self.B, x_0*px,y_0*px, 1, 1.0079, self.a, self.b))
                diff=np.sum(diff_arr)
                all_perr.append(diff)
                    
            #get all points of proton trace and append to list of arrays- important if there are interruptions
            x_sk=np.arange(skeleton.shape[1])*px #x values in um
            ion_trace=parabola_m(x_sk,E0,self.B, x_0*px,y_0*px, 1, 1.0079, self.a, self.b)
            trace_mask=np.zeros((skeleton.shape[0],skeleton.shape[1]),np.int8)
            width=5
            for i in np.arange(len(ion_trace)):
                j=int(round(ion_trace[i]/px,0))
                trace_mask[j-width:j+width,i]=1

            skeleton_proton_trace=cv2.bitwise_and(skeleton,skeleton,mask = trace_mask)
            points_proton_trace=img_to_points(skeleton_proton_trace,255)

            #append points of proton trace only
            all_x.append(points_proton_trace[:,0])
            all_y.append(points_proton_trace[:,1])
            if show_images and show_images_loop:
                plt.figure(figsize=(15,5))
                plt.imshow(crop,'gray',aspect='auto')
                plt.scatter(x_0,y_0,s=5,facecolors=None,edgecolors='r')
                plt.scatter(points_x,points_y,s=2)
                plt.scatter(points_proton_trace[:,0],points_proton_trace[:,1],s=2)
                plt.plot(x_sk/px,ion_trace/px,color='r')
                plt.title('show calculated proton trace')
                plt.show()
                
                # plt.figure(figsize=(15,5))
                # plt.plot(points_x*px,result.eval(x=points_x*px),color='#ff0000')
                # plt.scatter(points_x*px,points_y*px,s=1)#,facecolors=None,edgecolors='k'
                # plt.scatter(x_0*px,y_0*px)
                # plt.title(str(d)+' '+str(result.best_values['E_fit']))
                # plt.title('more protons?')
                # plt.show()
           

        ######### find optimum ##################
        print("> Find optimum")
        idx=all_perr.index(min(all_perr)) #optimal rotation angle has the lowest difference between optimal and measured proton trace
    
        #set values of optimum variables
        deg_opt=rot_angles[idx]
        rect_opt=all_rect[idx]
        points_x=all_x[idx]
        points_y=all_y[idx]
        points_y = [x for _,x in sorted(zip(points_x,points_y))]
        points_x = np.sort(points_x)

        self.rot_deg.append(deg_opt)
        self.crop_rect=rect_opt

        ############ write results to file #############
        print("> Write results to file")
        E_opt=all_params[idx]
        zero_opt=all_zeros[idx]
        #add margin for later on background subtraction
        zero_opt[1]=zero_opt[1]+margin_crop_rot
        
        self.efield=E_opt
        self.zero=zero_opt
       
        with open(output_path+file+"_fit_output.csv", 'w',newline='') as writeFile:
            writer = csv.writer(writeFile,delimiter=',')
            writer.writerow(["E-field",E_opt])
            writer.writerow(["Rotation angle",deg_opt])
            writer.writerow(['Zero',zero_opt])
            writer.writerow(["ROI",rect_opt])
            writer.writerow([])
            writer.writerow(["Points:"])
            writer.writerows([points_x,points_y])
       
        crop_opt=rotate(img,deg_opt,(img.shape[1]/2,img.shape[0]/2))
        #TODO: check if -100 in box
        crop_opt=crop_opt[rect_opt[1]:(rect_opt[1]+rect_opt[3])+margin_crop_rot,rect_opt[0]:(rect_opt[0]+rect_opt[2])]
        crop_opt=np.flipud(crop_opt)
        col_crop_opt=cv2.cvtColor(crop_opt,cv2.COLOR_GRAY2BGR)
        self.modified_IP = crop_opt
        cv2.imwrite(output_path+file+"_rotated_falseColors.tif",col_crop_opt)
        np.savetxt(output_path+self.filename+"_rotated.csv",crop_opt)
        
        #########-----PLOT----------############
        fig=plt.figure(figsize=(15, 8))
        fig.add_subplot(2,2,(1,2))
        plt.imshow(col_crop_opt, cmap = 'gray', interpolation = 'bicubic')
        #x_val=np.arange(100,3700)*px
        #plt.plot(x_val/px, col_crop_opt.shape[0]-(parabola_m(x_val,E_opt,zero[0],zero[1]))/px)
        #plt.plot(x_val/px, parabola_m(x_val,E_opt,zero[0]*px,zero[1]*px)/px)
        plt.scatter(points_x,[x+margin_crop_rot for x in points_y],s=1)
        
        fig.add_subplot(2,2,3)
        plt.scatter(rot_angles,all_perr,color="blue",label="std deviation fit")
        plt.scatter(deg_opt,all_perr[idx],color="red")
        plt.xlabel('Rotation degree in °')
        plt.ylabel(r'\Delta optimum-real')
        plt.legend(loc='best')
        
        fig.add_subplot(2,2,4)
        plt.scatter(rot_angles,all_params,color="blue",label="E-field")
        plt.scatter(deg_opt,all_params[idx],color="red")
        plt.xlabel('Rotation degree in °')
        plt.legend(loc='best')

        plt.savefig(output_path+self.filename+'_summary.png')
        plt.close()
        
        print("> Optimizing E-field and rotation finished.")
        return

    """MAIN FUNCTION 3

    Parameters
    ----------
    trace_width : width of ion traces along y axis
    offset_background : offset of background trace (above proton trace)
    charge : charge of the ion
    mass : mass of the ion in u, e.g. protons are given by 1 
    plotting functions are for debugging
    """
    def get_PSL_spectrum(self, trace_width=11,offset_background=-45, charge=1, mass=1.0079):   
        working_directory=self.path+'analysis/'
        img_psl=np.loadtxt(working_directory+self.filename+"_rotated.csv")
        self.modified_IP=img_psl
        plt.imshow(img_psl, aspect='auto')
        plt.title('PSL image')
        plt.show()

        #read file with fit parameters from previous analysis step
        print("> Read fit parameter")  
        try:               
            previous=[]
            print(working_directory+self.filename+"_fit_output.csv")
            with open(working_directory+self.filename+"_fit_output.csv", 'r') as readFile:
                reader = csv.reader(readFile,delimiter=',') 
                for i, line in enumerate(reader):
                    previous.append(line)
                #convert lines to respective value types and set necessary variables
                E_opt=float(previous[0][1])
                zero=previous[2][1].replace(',','').replace('[','').replace(']','').split()
                zero=[int(float(i)) for i in zero]
                self.zero=zero
                self.crop_rect=previous[3][1][1:-1].split(', ')
        except:
            print("!ERROR: Read output file of rotate_and_fit_E_field function failed!")
            return

        x=np.arange(img_psl.shape[1])*px #x values in um
        ion_trace=parabola_m(x,E_opt,self.B, zero[0]*px,zero[1]*px,charge,mass, self.a ,self.b) #calculate optimal proton parabola starting from previously determined zero
        
        # ---- create masks -------------
        # ideal_trace_mask: perfect parabola mask with trace width (function argument), which increases toward lower energies to include potential wiggles
        # realistic mask: centered around maximum value of trace along each x position
        # background mask: perfect parabola mask above the proton trace with an offset (argument of this function)
        # in these matrices: "i" corresponds to x, "j" corresponds to y
        print("> Create masks")
        ideal_trace_mask=np.zeros((img_psl.shape[0],img_psl.shape[1]),np.int8)
        for i in np.arange(len(ion_trace)):
            w=int(trace_width+(i/900)) #increase width for lower energies, since traces tend to wiggle more at low energies 
            j=int(round(ion_trace[i]/px,0))
            ideal_trace_mask[j-w:j+w,i]=1

        #find center of real trace - important for wiggely traces -> used to calculate a realistic mask
        idx_max=[]
        psl_trace_raw=cv2.bitwise_and(img_psl,img_psl,mask = ideal_trace_mask)
        for i in np.arange(len(ion_trace)):
            column=psl_trace_raw[:,i]
            indices=np.sort(np.where(column==np.amax(column,axis=0)))
            #if more than one pixel with max value (e.g. in saturated parts), calculate middle idx
            if len(indices[0])==1: idx=indices[0][0]
            else: idx=np.round(np.abs(indices[0][-1]+indices[0][0])/2)
            #check if idx is close enough to ideal trace. If not, use middle of ideal trace
            if idx in np.arange(int(np.round(ion_trace[i]/px))-int(trace_width),int(np.round(ion_trace[i]/px))+int(trace_width)):
                idx_max.append(idx)
            else:
                idx_max.append(int(np.round(ion_trace[i]/px)))
        if show_images:
            plt.imshow(img_psl, aspect='auto')
            plt.scatter(np.arange(len(ion_trace)),idx_max,s=1)
            plt.title('show ion trace along image')
            plt.show()

        realistic_mask=np.zeros((img_psl.shape[0],img_psl.shape[1]),np.int8)
        for i in np.arange(len(ion_trace)):
            j=int(idx_max[i])
            realistic_mask[j-trace_width:j+trace_width,i]=1
        if show_images:        
            fig=plt.figure(figsize=(15,10))
            fig.add_subplot(2,1,1)
            plt.imshow(img_psl, aspect = 'auto')
            fig.add_subplot(2,1,2)
            plt.imshow(realistic_mask, aspect='auto')
            plt.title('shows mask')
            plt.show()

        background_mask=np.zeros((img_psl.shape[0],img_psl.shape[1]),np.int8)
        for i in np.arange(len(ion_trace)):
            j=int(round(ion_trace[i]/px,0))
            background_mask[j-trace_width+offset_background:j+trace_width+offset_background,i]=1

        self.masks_for_spectra=[ideal_trace_mask,realistic_mask,background_mask,idx_max]

        plt.imshow(img_psl, aspect='auto')
        plt.plot(np.arange(img_psl.shape[1]),ion_trace/px,color='k',ls='--',linewidth=.1) #ideal trace
        plt.plot(np.arange(img_psl.shape[1]),[y-(trace_width+x/900) for x,y in zip(np.arange(len(ion_trace)),ion_trace/px)],color='k',linewidth=.1)
        plt.plot(np.arange(img_psl.shape[1]),[y+(trace_width+x/900) for x,y in zip(np.arange(len(ion_trace)),ion_trace/px)],color='k',linewidth=.1)
        plt.plot(np.arange(img_psl.shape[1]),idx_max,color='r',ls='--',linewidth=.1) #realistic trace used for spectrum extraction
        plt.plot(np.arange(img_psl.shape[1]),[y-(trace_width) for x,y in zip(np.arange(len(ion_trace)),idx_max)],color='r',linewidth=.1)
        plt.plot(np.arange(img_psl.shape[1]),[y+(trace_width) for x,y in zip(np.arange(len(ion_trace)),idx_max)],color='r',linewidth=.1)
        plt.plot(np.arange(img_psl.shape[1]),ion_trace/px+offset_background,color='b',ls='--',linewidth=.1) #background trace
        plt.plot(np.arange(img_psl.shape[1]),[y+(trace_width) for x,y in zip(np.arange(len(ion_trace)),ion_trace/px+offset_background)],color='b',linewidth=.1)
        plt.plot(np.arange(img_psl.shape[1]),[y-(trace_width) for x,y in zip(np.arange(len(ion_trace)),ion_trace/px+offset_background)],color='b',linewidth=.1)
        plt.scatter(zero[0],zero[1],s=5,color='r')
        plt.savefig(working_directory+self.filename+'_traces.png',dpi=800)
        if show_images:
            plt.title('shows psl image with traces')
            plt.show()
        plt.close()

        #### ---------- convert position to energy --------- ####
        print("> Get spectrum")
        # apply masks
        realistic_trace=cv2.bitwise_and(img_psl,img_psl,mask = realistic_mask)
        background_trace=cv2.bitwise_and(img_psl,img_psl,mask = background_mask)
        spectrum_PSL_x=np.sum(realistic_trace,axis=0)-np.sum(background_trace,axis=0) # PSL spectrum as a function of position (along x axis)   
        
        #save masked image - e.g. for scan recovery
        np.savetxt(working_directory+self.filename+'_proton_trace_masked.csv',realistic_trace)    
        if show_images:
            fig=plt.figure()
            plt.subplot(4,1,1)
            plt.imshow(realistic_trace, aspect='auto')
            plt.subplot(4,1,2)
            plt.imshow(background_trace, aspect='auto')
            plt.subplot(4,1,3)
            plt.imshow(img_psl, aspect='auto')
            plt.title('comparison of all traces')
            plt.show()
    
        # ------ PSL to PSL/MeV ----------------
        print("> Convert positions to energy bins")
        idx1=zero[0]+1
        x_val=x[idx1:]-zero[0]*px #get all relevant x values (starting from the zero of the parabola, "x" are the pixels of the entire image in um)
        energy_bins=energy_dispersion(x_val, self.B, charge, mass, self.b) #convert positions to respective energy (in MeV)

        #only keep values smaller than the given upper energy limit (defined at the top of this script)
        idx2=next(i for i in energy_bins if i < upper_limit)
        idx2=list(energy_bins).index(idx2)
        energy_bins=energy_bins[idx2:]
        x_positions=x_val[idx2:] #keep x positions that correspond to the desired energy range
        spectrum_PSL_x=spectrum_PSL_x[idx1+idx2:] #keep PSL values that are in the desired energy range
        #set properties
        self.energy_bins=energy_bins
        self.spectrum_PSL=spectrum_PSL_x
        self.x_positions=x_positions

        #the dispersion relation is non-linear, therefore the effective bin size varies and must be normalized
        print("> Normalize energy bins")    
        x_edges=np.append(x_val[idx2:],x_val[-1]+px)-(px/2) # create array with positions of "edges of px bins" 
        energy_edges=energy_dispersion(x_edges, self.B, charge, mass, self.b) # get energies for these edges
        energy_bin_widths=np.abs(np.diff(energy_edges)) # calculate bin width in units of energy
        spectrum_PSL_norm=spectrum_PSL_x/energy_bin_widths # divide PSL in each bin by bin width to get number per MeV
        #set properties
        self.energy_bin_widths=energy_bin_widths
        self.spectrum_PSL_normalized=spectrum_PSL_norm

        #create output plots
        fig=plt.figure(figsize=(15, 10))
        plt.subplots_adjust(hspace=1.2)
        plt.subplot(4,1,1)
        plt.scatter(x_val[idx2:],spectrum_PSL_x,s=1)
        plt.yscale('log')
        # plt.ylim(0.5,500)
        plt.xlabel('Pos in px')
        plt.ylabel('Number in PSL')
        plt.title('spectrum - as on IP')
        plt.subplot(4,1,2)
        plt.scatter(energy_bins,spectrum_PSL_x,s=1)
        plt.yscale('log')
        # plt.ylim(0.5,500)
        # plt.xlim(0,55)
        plt.xlabel('Energy in MeV')
        plt.ylabel('Number in PSL')
        plt.title('spectrum - raw')
        plt.subplot(4,1,3)
        plt.scatter(energy_bins,spectrum_PSL_norm,s=1)
        plt.yscale('log')        
        # plt.xlim(0,55)
        plt.xlabel('Energy in MeV')
        plt.ylabel('Number in PSL per MeV')
        plt.title('spectrum - PSL scaled')      
        # plt.ylim(10,300000)
        # plt.xlim(0,55)
        plt.subplot(4,1,4)
        plt.plot(energy_bins,energy_bin_widths)
        plt.xlabel('Energy in MeV')
        plt.ylabel('Bin width in MeV')
        plt.title('Energy bin widths')
        plt.savefig(working_directory+self.filename+'_spectra.png')
        plt.close()    

        #save spectra
        with open(working_directory+self.filename+"_spectrum.csv", 'w',newline='') as writeFile:
                writer = csv.writer(writeFile,delimiter=',')
                writer.writerow(["Energy bins in MeV","PSL Spectrum in PSL",'PSL Spectrum normalized in PSL/MeV','Energy bin width in MeV'])
                writer.writerows([self.energy_bins])
                writer.writerows([spectrum_PSL_x])
                writer.writerows([spectrum_PSL_norm])
                writer.writerows([self.energy_bin_widths])

        return
    
    """MAIN FUNCTION 4

    Parameters
    ----------
    time : time in minutes between shot and scan

    """
    def correct_fading(self,time):
        working_directory=self.path+'analysis/'
        self.spectrum_PSL_at_t0=fading_correction(self.spectrum_PSL_normalized,time,radiation_type='ion',IP_type=self.IP_type,data_dimension=1)
        #add to spectrum.csv by rewriting the file
        with open(working_directory+self.filename+"_spectrum.csv", 'w',newline='') as writeFile:
                writer = csv.writer(writeFile,delimiter=',')
                writer.writerow(["Energy bins in MeV","PSL Spectrum in PSL",'PSL Spectrum normalized in PSL/MeV','Energy bin width in MeV','PSL spectrum (norm.) at t0 in PSL/MeV'])
                writer.writerows([self.energy_bins])
                writer.writerows([self.spectrum_PSL])
                writer.writerows([self.spectrum_PSL_normalized])
                writer.writerows([self.energy_bin_widths])
                writer.writerows([self.spectrum_PSL_at_t0])
        return
        
    """
    MAIN FUNCTION 5

    Parameters
    ----------
    IP_filter : list of ['material',distance_to_IP_edge_start,distance_to_IP_edge_end,thickness] with distances in mm

    """
    def PSL_to_number(self,IP_filter=[['',0,0,0]], charge=1, mass = 1.0079):
        working_directory=self.path+'analysis/'
        energies=self.energy_bins
        #check for filter and apply if given
        if IP_filter[0]!=['',0,0,0]:
            for f in IP_filter:
                #calculate effective filter length by subtracting zero position
                zero_x=int(self.crop_rect[0])+self.zero[0]
                distance_min=(f[1]/1e3)-(zero_x*px) #in m
                distance_max=(f[2]/1e3)-(zero_x*px) #in m
                #convert distance to energy
                energy_min=energy_dispersion(distance_max, self.B, charge, mass, self.b)
                if distance_min<=0:
                    energy_max=max(self.energy_bins)
                else:
                    energy_max=energy_dispersion(distance_min, self.B, charge, mass, self.b)
                #apply filter to energy array
                energies=energy_loss_in_filter_range(energies[::-1],energy_min,energy_max,f[0],f[3])[::-1] #reverse array to get max to min values
        # if show_images:
        #     plt.plot(self.energy_bins,self.energy_bins)
        #     plt.plot(self.energy_bins,energies)
        #     plt.grid(True)
        #     plt.xlim(0,30)
        #     plt.axvline(energy_min)
        #     plt.axvline(energy_max)
        #     plt.title('show spectrum')
        #     plt.show()

        #conversion of PSL to protons; Rabhi 2017, doi.org/10.1063/1.5009472
        path_sensitivities='./tp_modules/IP_sensitivity/'
        if self.IP_type=='SR':
            sensitivity=np.loadtxt(path_sensitivities+'2017_Rabhi_SR_ImagePlateSensitivity.csv',skiprows=1,delimiter=';')
        elif self.IP_type=='MS':
            sensitivity=np.loadtxt(path_sensitivities+'2017_Rabhi_MS_ImagePlateSensitivity.csv',skiprows=1,delimiter=';')
        elif self.IP_type=='TR':
            sensitivity=np.loadtxt(path_sensitivities+'2017_Rabhi_TR_ImagePlateSensitivity.csv',skiprows=1,delimiter=';')
        else: 
            print('ERROR: IP type not valid.')
            return
        
        #sort array along first axis (to get sorted energy)
        sensitivity=sensitivity[sensitivity[:, 0].argsort()]
        energies_arange=np.arange(0.01,100,0.01)
        sensitivities_interp=np.interp(energies_arange,sensitivity[:,0],sensitivity[:,1])        
        energy_indices=searchsorted2(energies_arange, energies)
        if show_images:
            fig=plt.figure()
            fig.add_subplot(2,1,1)
            plt.plot(energies_arange,sensitivities_interp)
            plt.scatter(sensitivity[:,0],sensitivity[:,1],s=1,color='k')
            plt.yscale('log')
            plt.xlabel('energy in MeV')
            plt.title('sensitivity per MeV')
            fig.add_subplot(2,1,2)
            plt.plot(sensitivities_interp[energy_indices])
            plt.yscale('log')
            plt.title('sensitivity')
            plt.show()
        
        #get sensitivity array corresponding to the filtered energies
        sensitivities_for_filtered=sensitivities_interp[energy_indices]        
        
        #apply to PSL spectrum to get proton numbers
        self.spectrum_proton_numbers=self.spectrum_PSL_at_t0/sensitivities_for_filtered
        if show_images:
            plt.scatter(self.energy_bins,self.spectrum_proton_numbers,label='numbers',s=0.8)
            plt.scatter(self.energy_bins,self.spectrum_PSL_at_t0,label='PSL',s=0.8)
            energy_bins_idxs=searchsorted2(energies_arange,self.energy_bins)
            plt.scatter(self.energy_bins,self.spectrum_PSL_at_t0/sensitivities_interp[energy_bins_idxs],label='w/o filter',s=0.6)
            plt.yscale('log')
            plt.legend()
            plt.ylim(5,5e5)
            plt.xlim(2,35)
            plt.title('show spectrum')
            plt.show()
        
        #add to spectrum.csv by rewriting the file
        with open(working_directory+self.filename+"_spectrum.csv", 'w',newline='') as writeFile:
                writer = csv.writer(writeFile,delimiter=',')
                writer.writerow(["Energy bins in MeV","PSL Spectrum in PSL",'PSL Spectrum normalized in PSL/MeV','Energy bin width in MeV','PSL spectrum (norm.) at t0 in PSL/MeV','Protons per MeV'])
                writer.writerows([self.energy_bins])
                writer.writerows([self.spectrum_PSL])
                writer.writerows([self.spectrum_PSL_normalized])
                writer.writerows([self.energy_bin_widths])
                writer.writerows([self.spectrum_PSL_at_t0])
                writer.writerows([self.spectrum_proton_numbers])
        return
        
        

########### ----------- Helper Functions------------ ###################
#helper
def thresholding(img,thresh):
    retval, thresh_gray = cv2.threshold(img, thresh, maxval=255, type=cv2.THRESH_BINARY)
    thresh_gray=thresh_gray[1:thresh_gray.shape[0],1:thresh_gray.shape[1]] #remove first col and first row #TODO: check why this is necessary
    return thresh_gray

"""
convert white pixels of image (2D array) to set of (x,y) points
"""
def img_to_points(img,pt_color):
    points = np.argwhere(img==pt_color) # find where the white pixels are
    points = np.fliplr(points) # store them in x,y coordinates instead of row,col indices
    return points

#helper
def crop_img(img,points,delta_w,delta_h):
    x, y, w, h = cv2.boundingRect(points) # create a rectangle around the given points
    x, y, w, h = int(x-(delta_w/2)), int(y-(delta_h/2)), w+delta_w, h+delta_h # make the box a little bigger
    
    #if values are out of bounds (image size wise) set to smallest/largest possible value
    if x<0: 
        w=w-abs(x)
        x=0
    if y<0:
        h=h-abs(y)
        y=0
    if (x+w)>img.shape[1]: w=img.shape[1]-x #x=img.shape[0]-1
    if (y+h)>img.shape[0]: h=img.shape[0]-y #y=img.shape[1]-1

    crop = img[y:y+h, x:x+w]
    crop=np.flipud(crop)
    rect = (x,y,w,h) 
    return crop, rect

#helper
def rotate(img,deg,center):
    rows,cols = img.shape
    M = cv2.getRotationMatrix2D(center,deg,1)
    dst = cv2.warpAffine(img,M,(cols,rows),borderValue=0)#29.06. 255
    return dst

#helper
def rotate_PSL(img,deg,center):
    rows,cols=img.shape
    M = cv2.getRotationMatrix2D(center,deg,1.0)
    dst = cv2.warpAffine(img,M,(cols,rows),borderValue=0.0,flags=cv2.INTER_NEAREST)#cv2.INTER_CUBIC
    return dst

"""
Use Hough transformation to find possible (round) zeros of
Thomson parabola trace (infinite energy)
"""
def find_zero(img):
    img=img.astype(np.uint8)
    img=cv2.bitwise_not(img)
    color_img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR) #create color image (in a grayscale image no colored objects can be drawn...
    circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,param1=60,param2=2,minRadius=2,maxRadius=30)    #30.06. minRadius=0 # 07.07. param2=10
    circles = np.uint16(np.around(circles))
    for i in circles[0,:]:
        if(i[0]<img.shape[1]/9 and i[2]<30 and i[0] > 30): #check that zero is in left corner and size is not too large #30.06. and i[0] > 30 hinzugefügt
            # draw the outer circle
            cv2.circle(color_img,(i[0],i[1]),i[2],(0,255,0),2)
            # draw the center of the circle
            cv2.circle(color_img,(i[0],i[1]),2,(0,0,255),3)
            zero=i    
    plt.imshow(color_img,'gray')
    plt.title('image')
    plt.show()
    return zero, color_img

"""
find zeros of Thomson parabola trace using openCV findContours()
"""
def find_zero_cnt(img):
    img=img.astype(np.uint8)
    color_img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR) #create color image (in a grayscale image no colored objects can be drawn...)
    img_cnt=img.copy()
    zero=[0,0]
    zero_found = False
    contours,hierarchy=cv2.findContours(img_cnt,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)[-2:] #the [-2:] makes sure this line works for all cv2 versions - in some cases the return statements differ 
    for cnt in contours:
        area = cv2.contourArea(cnt)
        x,y,w,h=cv2.boundingRect(cnt)
        equi_diameter = np.sqrt(4*area/np.pi) #diameter of circle with eequivalent area
        if(x<img.shape[1]/9 and equi_diameter<30 and equi_diameter > 2 and not(zero_found)): #check that zero is in left corner and size is not too large #30.06. and i[0] > 30 hinzugefügt
            # draw the contour boundary
            cv2.drawContours(color_img, [cnt], 0, (0,255,0), 2)
            zero=[(2*x+w)/2,(2*y+h)/2]
            zero_found = True
    return zero, color_img

"""
convert spatial distance/reflection to energy -- relativistic, in m
"""
def energy_dispersion(x, B, charge, mass, b):
    m = const.m_p*mass
    q = const.e*charge
    c1=q*B*b/np.sqrt(2*m)
    energy=np.sqrt((m*c*c*2*c1*c1/(x*x))+(m*c*c)*(m*c*c))-m*c*c
    energy=energy/1000000/q #in MeV
    return energy

"""
derivative of energy dispersion function
"""
def dE_dx(x,delta_x, B, charge, mass, b):
    m = const.m_p*mass
    q = const.e*charge
    c1=q*B*b/np.sqrt(2*m)
    delta_E=(-2*c*c*c1*m/(x*x*x)/np.sqrt((m*c*c)*(m*c*c)+(2*m*c*c*c1)/(x*x)))*delta_x
    delta_E=delta_E/1000000/q #in MeV
    return delta_E

"""
fit function for Thomson parabola
all parameters in m
"""
def parabola_m(x_m, E_fit, B, x0, y0, charge, mass, a, b): #all parameters in m
    m = const.m_p*mass
    q = const.e*charge
    x_m=x_m+x0
    c3=np.power((q*B*b/np.sqrt(2*m)),-2)*np.power((q*E_fit*a/2),2)/(m*c*c)
    c4=np.power(q*B*b/np.sqrt(2*m),-2)*q*E_fit*a/2
    return y0+np.power((x_m),2)*(c4/2+np.sqrt(np.power(c4,2)/4+c3/np.power((x_m),2)))

"""
find points of proton trace
"""
def find_proton_trace(skeleton):
    kernel = np.ones((8,8), np.uint8) 
    img_contours=cv2.dilate(skeleton.copy(),kernel,iterations=1)
    contours,hierarchy=cv2.findContours(img_contours,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)[-2:] #the [-2:] makes sure this line works for all cv2 versions - in some cases the return statements differ 
    contours = list(contours)
    contours.sort(key=len) #sort contours by length, take last element (largest) as proton trace
    #plot contours
    img_with_contours=img_contours.copy()
    img_with_contours=cv2.cvtColor(img_with_contours,cv2.COLOR_GRAY2RGB)
    for cnt in contours:
        cv2.drawContours(img_with_contours, cnt, -1, (0,255,0), 3)
    #cv2.drawContours(img_with_contours, [contours[-1]], -1, (0,0,255), 3)
    if show_images and show_images_loop:
        plt.imshow(img_with_contours, aspect='auto')
        plt.title('img with contours')
        plt.show()
                
    mask=np.zeros(skeleton.shape,dtype=np.uint8) #create mask
    
    img_contours=None #delete image from memory
    
    cv2.drawContours(mask,[contours[-1]],-1,255,cv2.FILLED)
    skeleton_points=cv2.bitwise_and(skeleton,skeleton,mask=mask)
    points_sk = img_to_points(skeleton_points,255)
    points_x = points_sk[:,0]
    points_y = points_sk[:,1]
    
    return points_x, points_y

#helper
def power_law_fct(x,a,b,c,d):
    return a*x**-b+c*np.sqrt(x)+d

"""
https://stackoverflow.com/questions/20780017/vectorize-finding-closest-value-in-an-array-for-each-element-in-another-array
"""
def searchsorted2(known_array, test_array):
    index_sorted = np.argsort(known_array)
    known_array_sorted = known_array[index_sorted]
    known_array_middles = known_array_sorted[1:] - np.diff(known_array_sorted.astype('f'))/2
    idx1 = np.searchsorted(known_array_middles, test_array)
    indices = index_sorted[idx1]
    return indices
