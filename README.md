# thomson_analysis_python

The scripts in this project can be used to **analyze ion traces** that were recorded with **Thomson parabolas** using **images plates** as detector.

### Requirements
python==3.9  
lmfit==1.0.3  
matplotlib==3.5.2  
numpy==1.22.3  
opencv_contrib_python==4.5.3.56  
scipy==1.8.1  

If you are using different versions of the listed modules, there might be some compatibility issues.

### Project structure
The folder **tp_modules** contains four scripts: 
1. `tp_analysis.py`  

   Main analysis script. Defines a class for thomson parabola spectrum objects. The five main functions select the image plate from the raw scanned image, rotate the plate to fit a proton parabola, convert the trace on the plate to a spectrum, correct fading effects and convert the PSL values to proton numbers.

2. `ip_scanner.py`  

   Contains the scanner class, which allows to set the scanning parameters (resolution, latitude, sensitivity, ...). Used for IP fading correction, converting the bit images to PSL and scan recovery after multiple scans.

3. `tp_ip_filter.py`  

   The functions in this script calculate the energy loss of incoming particles in filters (made of e.g. Al or Cu) based on SRIM tables. They are currently used during the conversion step from PSL to proton numbers.

4. `write_and_load_config.py`

   Contains functions for writing and loading configuration files for the respective data sets, which simplifies batch usage. The configuration files contain all metadata required to analyze each specific image plate.

This folder also contains an `__init__.py` (which is required to allow this subfolder to be used as a python module) and two subfolders, which contain calibration data for specific IP types (*IP_sensitivity*) and filter materials (*SRIM_outputs*).

### Example of use

The script `P190_thomson_evaluation.py` is an example for the usage of this project. It imports the relevant functions from the *tp_modules* scripts and reads the metadata of the example data from the *P190_YannikZobus.txt* config file in the *config_files* subfolder. The example IP data is read from the *example_data* subfolder, in which the *analysis* folder is added during the script run time. Here, it already contains the evaluated data to show the results of the scripts. For this example, the bit image was converted to PSL beforehand using imageJ.

## Authors
The scripts of this project were developed by Tina Ebert (tebert@ikp.tu-darmstadt.de) and Jonas Kohl (jkohl@ikp.tu-darmstadt.de).
