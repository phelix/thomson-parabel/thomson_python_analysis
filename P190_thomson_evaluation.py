"""
Created July 2021

@author: Tina Ebert, tebert@ikp.tu-darmstadt.de, Jonas Kohl(jkohl@ikp.tu-darmstadt.de), Johannes Hornung (j.hornung@gsi.de)
"""

import os
import pathlib # for Python > v3.4
from tp_modules.tp_analysis import TP_Spectrum
from tp_modules.write_and_load_config import config
import tp_modules.ip_scanner as ip
from tp_modules.ip_scanner import Scan


##### -------- ROUTINE ----------
# 1. convert to PSL if necessary
# 2. fit E-field
# 3. get spectrum
# 4. IP fading correction
# 5. scan recovery if necessary (see routines in IP_scanner.py and PhD thesis of D. Rusby, p. 72ff.)
# 6. PSL to proton numbers, including filters

##### -------- MAIN --------------

## (1) Load PSL files
config_path = ".\\config_files\\"
config_name = "P190_YannikZobus.txt"


### what charge and mass are you looking for?
charge = 4 # in units of elemental charge
mass = 1.0079*12 # in units of u, e.g. 1.0079 correspond to protons

#create reader object that reads the information from the config file 
reader = config()
#create data dictionary
data_all = reader.load_from_txt(config_path,config_name)

def main():
    for d in data_all:
        # convert image to PSL if necessary
        if not(os.path.isfile(d["path"]+d['filename']+'_PSL.tif')):
            print('#-- converting '+str(d['filename'])+ ' to PSL')
            scan = Scan.init_from_file(d["path"]+d['filename'], compression_type='exp')
            scan.convert_and_save_psl()

        # load the trace and prepare the TP_Spectrum
        print('#-- initializing '+str(d['filename']))
        shot=TP_Spectrum(d["path"],d['filename']+'_PSL',d['IP'],d['E-field'],d['E-length'],d['E-drift'],d['B-field'],d['B-length'],d['B-drift'])
        
        #create analysis folder
        pathlib.Path(d["path"]+'analysis/').mkdir(exist_ok=True)
        
        #select and crop IP        
        print('#-- crop IP')
        if os.path.isfile(shot.path+'analysis/'+shot.filename+'_cropped_IP.tif'):
            print('> cropped file already exists')
        else:
            shot.crop_IP(thresh=d['thresh_IP'],rotation=d['rot_deg'])
        
        print('#-- fit E-field and optimize rotation angle')
        #find optimal rotation angle (and fit E-field (optional))
        if os.path.isfile(shot.path+'analysis/'+shot.filename+"_fit_output.csv"):
            print('> E-field and rotation angle already optimized')
        else:
            shot.rotate_and_fit_E_field(thresh=d['thresh_trace'],fit=False)
        
        #get PSL spectrum, normalized to PSL/MeV 
        print('#-- get spectrum')
        shot.get_PSL_spectrum(charge=charge, mass=mass)

        #correct PSL values for fading (time in minutes)    
        print('#-- correct fading')
        shot.correct_fading(d['time'])
        
        #convert corrected PSL values to particle numbers, considering potential filters in front of the IP
        print('#-- convert PSL to particle number')
        shot.PSL_to_number(d['filter'], charge=charge, mass=mass)
            
        print('#-- finished '+str(d['filename']))
        print('----------------------------')
        
 
############## start main function
main()
