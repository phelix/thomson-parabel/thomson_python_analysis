import numpy as np
import PIL
from PIL import Image

file_name = 'test' # this is the file name of the scan without any file type

func_var = 'log' # or 'root'

# read the .inf file containing the scan infos
inf_file = open(file_name+'.inf', 'r')
lines = inf_file.readlines()

count = 1
relevant_parts = []
# get the relevant lines from the .inf file
for line in lines:
    if count > 3 and count < 11:
        relevant_parts.append(line.strip())
    count += 1

relevant_parts =  [int(x) for x in relevant_parts]
size_x = relevant_parts[3] # size of the scanned image in x-direction
size_y = relevant_parts[4] # size of the scanned image in y-direction
resolution = relevant_parts[0] # scanner resolution
S = relevant_parts[5] # sensitivity
bits = relevant_parts[2] # bit depth
L = relevant_parts[6] # latitude

# calculate the maximum aplitude from the bit depth
G=pow(2,bits)-1

# open the .img file
fid = open(file_name+'.img', 'rb')
data = np.fromfile(fid, np.dtype('>u2'))
im_array = data.reshape((size_y,size_x))

# convert to PSL value, mainly log, e.g. 'exp' is being used
if (func_var=='root'):
    im_PSL_array = pow(im_array/G,2)*pow(resolution/100,2)*pow(10,L/2)*(4000/S)

if (func_var=='log'):
    im_PSL_array = pow(resolution/100,2)*pow(10,L*(im_array/G-0.5))*(4000/S)

# save the PSL image
im_PSL = Image.fromarray(im_PSL_array)
im_PSL.save(file_name + "_PSL.tif", "TIFF")