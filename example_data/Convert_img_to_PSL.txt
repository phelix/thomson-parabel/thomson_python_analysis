macro "Open inf+img... NEW [k]" {

leftButton=16;
a=File.openAsString("");
b=split(a,"\n");
imgFilename=substring(File.name,0,lengthOf(File.name)-4)
sizeX=toString(parseInt(b[6]));
sizeY=toString(parseInt(b[7]));
run("Raw...", "open=["+File.directory+imgFilename+".img] image=[16-bit Unsigned] width="+sizeX+" height="+sizeY+" offset=0 number=1 gap=0"); run("32-bit"); run("Fire"); run("Enhance Contrast", "saturated=0.5"); Res=parseInt(b[3]); S=parseInt(b[8]); L=parseInt(b[9]); G=pow(2,parseInt(b[5]))-1;

Dialog.create("Convert inf+img to PSL"); Dialog.addMessage("Do you want to convert to PSL?"); Dialog.show();

Dialog.create("Which conversion would you like to use?");
     var func_var = newArray("log", "root"); Dialog.addChoice("Which Data Type was set in the preferences?",func_var,"log");
Dialog.addMessage("(c)2019 Steffen Sander \n s.sander@gsi.de \n"); Dialog.show();

func_var = Dialog.getChoice();

if (func_var=="root") func=1;
if (func_var=="log") func=2;


w = getWidth(); h = getHeight();
if (func==1)     for (y=0; y<h; y++) {
                     for (x=0; x<w; x++){ setPixel(x,y,pow((getPixel(x,y)/G),2)*pow((Res/100),2)*pow(10,(L/2))*(4000/S));
                         }
                     if (y%10==0) showProgress(y, h);
                 };
if (func==2)     for (y=0; y<h; y++) {
                     for (x=0; x<w; x++){ setPixel(x,y,pow((Res/100),2)*(4000/S)*pow(10,L*((getPixel(x, y)/G)-0.5)));
                           };
                     if (y%10==0) showProgress(y, h);
                 };



run("Enhance Contrast", "saturated=0.5"); // run("Set Scale...", "distance=1 known="+Res+" pixel=1 unit= m"); Dialog.create("Save as tif?"); Dialog.show(); saveAs("Tiff", File.directory+imgFilename+"_PSL.tif");

}
